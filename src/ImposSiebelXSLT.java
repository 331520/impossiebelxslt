
import com.siebel.data.SiebelPropertySet;
import com.siebel.eai.SiebelBusinessServiceException;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author IEUser
 */
public class ImposSiebelXSLT extends com.siebel.eai.SiebelBusinessService {

    public void doInvokeMethod(String methodName, SiebelPropertySet input,
            SiebelPropertySet output) throws SiebelBusinessServiceException {

        String X = input.getProperty("X");
        String Y = input.getProperty("Y");
        JFrame1 frame1 = new JFrame1();
        

        PrintWriter writer = null;
        try {
            writer = new PrintWriter("JBS.log", "UTF-8");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ImposSiebelXSLT.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ImposSiebelXSLT.class.getName()).log(Level.SEVERE, null, ex);
        }
        writer.println("The first line " + X);
        writer.println("The second line " + Y);
        frame1.runForm();
        

        if (X == null || X.equals("") || (Y == null) || Y.equals("")) {
            throw new SiebelBusinessServiceException("NO_PAR", "Missing param");
        }

        if (!methodName.equals("Add")) {
            throw new SiebelBusinessServiceException("NO_SUCH_METHOD", "No such method");
        } else {

            int x = 0;
            int y = 0;
            try {
                x = Integer.parseInt(X);
                y = Integer.parseInt(Y);
            } catch (NumberFormatException e) {
                throw new SiebelBusinessServiceException("NOT_INT", "Noninteger passed");
            }
            int z = x + y;
            writer.println("x + y = " + z);
            writer.close();
            output.setProperty("Z", new Integer(z).toString());
        }
    }
}
